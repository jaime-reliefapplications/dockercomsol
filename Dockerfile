FROM debian:latest
 
RUN apt-get update && apt-get install -y x11-apps
RUN rm -rf /tmp/* /usr/share/doc/* /usr/share/info/* /var/tmp/*
RUN useradd -ms /bin/bash user
ENV DISPLAY :0
 

COPY . /code/
RUN chmod +x code/2022.03.04_DigitalTwinAppleV3.sh

USER user
# This command will run the script during build, but none work
# RUN ./code/2022.03.04_DigitalTwinAppleV3.sh

ENTRYPOINT ["/bin/bash", "-c", "$0 \"$@\"", "./code/2022.03.04_DigitalTwinAppleV3.sh"]